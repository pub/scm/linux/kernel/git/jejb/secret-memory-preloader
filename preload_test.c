#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <openssl/crypto.h>

#define CHUNK_SIZE 256
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

static void *myalloc(void)
{
	size_t size = (random() &0xfff);
	return OPENSSL_malloc(size);
}

int main(int argc, const char *argv[])
{
	void *mem[50];
	int i;

	printf("\nAlloc array\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i++)
		mem[i] = myalloc();

	printf("\nFree array ascending\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i++)
		OPENSSL_free(mem[i]);

	printf("\nAlloc array\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i++)
		mem[i] = myalloc();

	printf("\nFree array descending\n\n");
	for (i = ARRAY_SIZE(mem) - 1; i >=0;  i--)
		OPENSSL_free(mem[i]);

	printf("\nAlloc array\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i++)
		mem[i] = myalloc();

	printf("\nFree *2\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);
	printf("\nFree holes\n\n");
	for (i = 1; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);

	
	printf("\nAlloc array for split\n\n");
	mem[1] = OPENSSL_malloc(0x10000);
	mem[0] = OPENSSL_malloc(1);
	OPENSSL_free(mem[1]);

	for (i = 1; i < ARRAY_SIZE(mem); i++)
		mem[i] = myalloc();

	printf("\nFree *2\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);
	printf("\nFree holes\n\n");
	for (i = 1; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);

	
	printf("\nForce Allocation across the segment boundary\n\n");
	mem[0] = OPENSSL_malloc(0x1ff000);

	for (i = 1; i < ARRAY_SIZE(mem); i++)
		mem[i] = myalloc();

	printf("\nFree *2\n\n");
	for (i = 0; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);
	printf("\nFree holes\n\n");
	for (i = 1; i < ARRAY_SIZE(mem); i += 2)
		OPENSSL_free(mem[i]);

	printf("\nFail oversize allocation\n\n");
	mem[0] = OPENSSL_malloc(2*1024*1024);
	if (mem[0] != NULL)
		printf("FAILED\n");

	printf("\n100 Random allocations and frees\n\n");

	memset(mem, 0, sizeof(mem));

	for (i = 0; i < 300; i++) {
		int j = random()%ARRAY_SIZE(mem);
		if (mem[j]) {
			OPENSSL_free(mem[j]);
			mem[j] = NULL;
		} else {
			mem[j] = myalloc();
		}
	}
	for (i = 0; i < ARRAY_SIZE(mem); i++)
		OPENSSL_free(mem[i]);

	exit(0);
}
	
